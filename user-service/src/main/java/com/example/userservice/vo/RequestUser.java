package com.example.userservice.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RequestUser {

    @NotNull(message = "email can not be null")
    @Size(min = 2, message = "email not be less than two characters")
    private String email;

    @NotNull(message = "name can not be null")
    @Size(min = 2, message = "name not be less than two characters")
    private String name;

    @NotNull(message = "password can not be null")
    @Size(min = 4, message = "password must be equal or grater than four characters")
    private String pwd;
}
