package com.example.orderservice.controller;

import com.example.orderservice.dto.OrderDto;
import com.example.orderservice.entity.OrderEntity;
import com.example.orderservice.messagequeue.KafkaProducer;
import com.example.orderservice.messagequeue.OrderProducer;
import com.example.orderservice.service.OrderService;
import com.example.orderservice.vo.RequestOrder;
import com.example.orderservice.vo.ResponseOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/order-service")
@RequiredArgsConstructor
@Slf4j
public class OrderController {

    private final Environment env;
    private final OrderService orderService;
    private final KafkaProducer kafkaProducer;
    private final OrderProducer orderProducer;

    @GetMapping("/health_check")
    public String status() {
        return String.format("It's working in order service on port %s", env.getProperty("local.server.port"));
    }


    @PostMapping("/{userId}/orders")
    public ResponseEntity<?> createOrder(@RequestBody RequestOrder requestOrder, @PathVariable String userId){
        log.info("Before add orders data======*******=========");
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        OrderDto orderDto = mapper.map(requestOrder, OrderDto.class);
        orderDto.setUserId(userId);

        // jpa
        orderService.createOrder(orderDto);
        ResponseOrder responseUser = mapper.map(orderDto, ResponseOrder.class);


//        orderDto.setOrderId(UUID.randomUUID().toString());
//        orderDto.setTotalPrice(requestOrder.getQty() * requestOrder.getUnitPrice());


        // kafka 메시지 전달(catalog)
        kafkaProducer.send("example-catalog-topic", orderDto);
        //orderProducer.send("orders", orderDto);

       // ResponseOrder responseUser = mapper.map(orderDto, ResponseOrder.class);
        log.info("After add orders data======*******=========");
        return ResponseEntity.status(HttpStatus.CREATED).body(responseUser);
    }


    @GetMapping("/{userId}/orders")
    public ResponseEntity<?> getOrders(@PathVariable String userId) throws Exception {
        log.info("Before call orders data===============");
        Iterable<OrderEntity> orderList = orderService.getOrdersByUserId(userId);
        List<ResponseOrder> result = new ArrayList<>();
        orderList.forEach(v -> {
            result.add(new ModelMapper().map(v, ResponseOrder.class));
        });

        /* zipkin 에러 발생 테스트 */
//        try {
//            Thread.sleep(1000);
//            throw new Exception("에러 발생");
//        } catch(InterruptedException e) {
//            log.warn(e.getMessage());
//        }

        log.info("After call orders data===============");
        return  ResponseEntity.status(HttpStatus.OK).body(result);
    }

}
